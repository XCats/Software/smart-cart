package frc.robot.components;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.DigitalInput;

public class CrashCartMotor {

    private CANSparkMax motor;
    private DigitalInput invert;
    private CrashCartController controller;

    public CrashCartMotor(int motorID, int invertChannel, CrashCartController defaultController) {
        this.motor = new CANSparkMax(motorID, CANSparkMaxLowLevel.MotorType.kBrushless);
        this.invert = new DigitalInput(invertChannel);
        this.controller = defaultController;
    }

    public void setController(CrashCartController controller) {
        this.controller = controller;
    }

    public void periodic() {
        motor.set(this.controller.getSpeed());
    }

}
