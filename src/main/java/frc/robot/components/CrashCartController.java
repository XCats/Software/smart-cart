package frc.robot.components;

import edu.wpi.first.wpilibj.AnalogPotentiometer;
import frc.robot.Constants;

public class CrashCartController {

    private AnalogPotentiometer input;

    public CrashCartController(int inputChannel) {
        this.input = new AnalogPotentiometer(inputChannel);
    }

    public double getSpeed() {
        return this.input.get() * Constants.SPEED_POT_CONVERSION_FACTOR;
    }


}
